﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace GameWithAnimations
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnColor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnLeft { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnLeftDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnLeftUp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnRight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnRightDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnRigthUp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnRotate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnScaleMinus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnScalePlus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnTransform { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnUp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _field { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _figFive { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _figFour { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _figOne { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _figSix { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _figThree { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _figTwo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _finishImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _seryozha { get; set; }

        [Action ("_btnColor_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnColor_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnDown_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnDown_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnLeft_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnLeft_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnLeftDown_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnLeftDown_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnLeftUp_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnLeftUp_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnRight_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnRight_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnRightDown_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnRightDown_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnRigthUp_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnRigthUp_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnRotate_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnRotate_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnScaleMinus_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnScaleMinus_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnScalePlus_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnScalePlus_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnTransform_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnTransform_TouchUpInside (UIKit.UIButton sender);

        [Action ("_btnUp_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void _btnUp_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (_btnColor != null) {
                _btnColor.Dispose ();
                _btnColor = null;
            }

            if (_btnDown != null) {
                _btnDown.Dispose ();
                _btnDown = null;
            }

            if (_btnLeft != null) {
                _btnLeft.Dispose ();
                _btnLeft = null;
            }

            if (_btnLeftDown != null) {
                _btnLeftDown.Dispose ();
                _btnLeftDown = null;
            }

            if (_btnLeftUp != null) {
                _btnLeftUp.Dispose ();
                _btnLeftUp = null;
            }

            if (_btnRight != null) {
                _btnRight.Dispose ();
                _btnRight = null;
            }

            if (_btnRightDown != null) {
                _btnRightDown.Dispose ();
                _btnRightDown = null;
            }

            if (_btnRigthUp != null) {
                _btnRigthUp.Dispose ();
                _btnRigthUp = null;
            }

            if (_btnRotate != null) {
                _btnRotate.Dispose ();
                _btnRotate = null;
            }

            if (_btnScaleMinus != null) {
                _btnScaleMinus.Dispose ();
                _btnScaleMinus = null;
            }

            if (_btnScalePlus != null) {
                _btnScalePlus.Dispose ();
                _btnScalePlus = null;
            }

            if (_btnTransform != null) {
                _btnTransform.Dispose ();
                _btnTransform = null;
            }

            if (_btnUp != null) {
                _btnUp.Dispose ();
                _btnUp = null;
            }

            if (_field != null) {
                _field.Dispose ();
                _field = null;
            }

            if (_figFive != null) {
                _figFive.Dispose ();
                _figFive = null;
            }

            if (_figFour != null) {
                _figFour.Dispose ();
                _figFour = null;
            }

            if (_figOne != null) {
                _figOne.Dispose ();
                _figOne = null;
            }

            if (_figSix != null) {
                _figSix.Dispose ();
                _figSix = null;
            }

            if (_figThree != null) {
                _figThree.Dispose ();
                _figThree = null;
            }

            if (_figTwo != null) {
                _figTwo.Dispose ();
                _figTwo = null;
            }

            if (_finishImage != null) {
                _finishImage.Dispose ();
                _finishImage = null;
            }

            if (_seryozha != null) {
                _seryozha.Dispose ();
                _seryozha = null;
            }
        }
    }
}