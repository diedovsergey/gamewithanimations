﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace GameWithAnimations
{
    public class TriangleView: UIView
    {
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            using (CGContext g = UIGraphics.GetCurrentContext())
            {

                //set up drawing attributes
                UIColor.FromRGBA(20, 100, 40, 255).SetFill();

                //create geometry
                var path = new CGPath();

                path.AddLines(new CGPoint[]{
                new CGPoint (60, 0),
                new CGPoint (0, 60),
                new CGPoint (60, 60)});

                path.CloseSubpath();

                //add geometry to graphics context and draw it
                g.AddPath(path);
                g.DrawPath(CGPathDrawingMode.Fill);
            }
        }
    }
}