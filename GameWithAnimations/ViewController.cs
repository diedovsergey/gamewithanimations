﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using System.Threading;
using System.Threading.Tasks;

namespace GameWithAnimations
{
    public partial class ViewController : UIViewController
    {
        private float _step = 10f;
        private float _rotation = 0;
        private bool _isRect;
        private float _scale = 1f;
        private CGRect _frame;
        private CGPoint _center;
        private CGAffineTransform _transform;
        private float _v = 2f;
        private bool _isRound;
        UIView _rect;
        UIView _drawTriangle;

        NSTimer gameTimer;
        int count = 0;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _frame = _seryozha.Frame;
            _field.Layer.BorderWidth = 5;
            _field.Layer.BorderColor = UIColor.Green.CGColor;
            _center = _seryozha.Center;
            _seryozha.BackgroundColor = UIColor.Red;
            _seryozha.Layer.BorderWidth = 2;
            _seryozha.Layer.BorderColor = UIColor.Black.CGColor;
            _transform = CGAffineTransform.MakeIdentity();
            CreateLab();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        #region Figures

        private void CreateLab ()
        {
            _rect = new UIView(new CGRect(100, 30, 20, 30));
            
            _figThree.Layer.CornerRadius = _figThree.Frame.Width / 2;
            _figFour.Layer.CornerRadius = _figFour.Frame.Height / 2;
            _figFive.Layer.CornerRadius = _figFour.Frame.Height / 2;
            _figSix.Layer.CornerRadius = _figFour.Frame.Height / 2;

            _rect.Transform = CGAffineTransform.MakeRotation(0.75f);
            _rect.BackgroundColor = UIColor.Black;

            _figFive.Transform = CGAffineTransform.MakeRotation(0.3f);
            _figSix.Transform = CGAffineTransform.MakeRotation(-0.5f);

            _drawTriangle = new TriangleView
            {
                Frame = new CGRect(60, 340, 60, 60),
                BackgroundColor = UIColor.FromRGBA(140, 255, 200, 0)
            };

            gameTimer = NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(1.0), delegate { MoveFigure(); });
            
            View.AddSubviews(_drawTriangle, _rect);
        }

        private void MoveFigure()
        {
            count++;

            if (_isRound)
            {
                _isRound = false;
                UIView.Animate(0.3f, () =>
                {
                    _figThree.Layer.CornerRadius = 0;
                    _figFive.Transform = CGAffineTransform.MakeRotation(0.3f);
                    _figSix.Transform = CGAffineTransform.MakeRotation(-0.5f);
                });
            }
            else
            {
                _isRound = true;
                UIView.Animate(0.3f, () =>
                {
                    _figThree.Layer.CornerRadius = _figThree.Frame.Width / 2;
                    _figFive.Transform = CGAffineTransform.MakeRotation(-0.3f);
                    _figSix.Transform = CGAffineTransform.MakeRotation(0.5f);
                });
            }
        }

        #endregion

        #region Move buttons

        partial void _btnUp_TouchUpInside(UIButton sender)
        {
            if (_center.Y - _seryozha.Layer.Frame.Height / 2 >= _field.Frame.Top + 10)
                UIView.Animate(0.2f, () =>
                {
                    _center.Y -= _step;
                    _seryozha.Center = _center;
                });
        }

        partial void _btnDown_TouchUpInside(UIButton sender)
        {
            if (_center.Y + _seryozha.Layer.Frame.Height / 2 <= _field.Frame.Bottom - 40)
                UIView.Animate(0.2f, () =>
                {
                    _center.Y += _step;
                    _seryozha.Center = _center;
                });
        }

        partial void _btnLeft_TouchUpInside(UIButton sender)
        {
            if (_center.X - _seryozha.Layer.Frame.Width / 2 >= _field.Frame.Left + 25)
                UIView.Animate(0.2f, () =>
                {
                    _center.X -= _step;
                    _seryozha.Center = _center;
                });
        }

        partial void _btnRight_TouchUpInside(UIButton sender)
        {
            if (_center.X + _seryozha.Layer.Frame.Width /2 <= _field.Frame.Right - 10)
                UIView.Animate(0.2f, () =>
                {
                    _center.X += _step;
                    _seryozha.Center = _center;
                });
        }

        partial void _btnLeftUp_TouchUpInside(UIButton sender)
        {
            if (_center.Y - _seryozha.Layer.Frame.Height / 2 >= _field.Frame.Top + 10 && _center.X - _seryozha.Layer.Frame.Width / 2 >= _field.Frame.Left + 30)
                UIView.Animate(0.2f, () =>
                {
                    _center.X -= _step;
                    _center.Y -= _step;
                    _seryozha.Center = _center;
                });
        }

        partial void _btnRigthUp_TouchUpInside(UIButton sender)
        {
            if(_center.Y - _seryozha.Layer.Frame.Height / 2 >= _field.Frame.Top + 10 && _center.X + _seryozha.Layer.Frame.Width / 2 <= _field.Frame.Width - 10)
            UIView.Animate(0.2f, () =>
            {
                _center.X += _step;
                _center.Y -= _step;
                _seryozha.Center = _center;
            });
        }

        partial void _btnLeftDown_TouchUpInside(UIButton sender)
        {
            if(_center.Y + _seryozha.Layer.Frame.Height / 2 <= _field.Frame.Height - 30 && _center.X - _seryozha.Layer.Frame.Width / 2 >= _field.Frame.Left + 30)
            UIView.Animate(0.2f, () =>
            {
                _center.X -= _step;
                _center.Y += _step;
                _seryozha.Center = _center;
            });
        }

        partial void _btnRightDown_TouchUpInside(UIButton sender)
        {
            if(_center.Y + _seryozha.Layer.Frame.Height / 2 <= _field.Frame.Height - 30 && _center.X + _seryozha.Layer.Frame.Width / 2 <= _field.Frame.Right - 10)
            UIView.Animate(0.2f, () =>
            {
                _center.X += _step;
                _center.Y += _step;
                _seryozha.Center = _center;
            });
        }

        #endregion

        #region Transform buttons

        partial void _btnScalePlus_TouchUpInside(UIButton sender)
        {
            _scale += 0.2f;
            _v += 0.4f;

            UIView.Animate(0.5f, () =>
            {
                _transform = CGAffineTransform.MakeScale(_scale, _scale);
                _seryozha.Transform = _transform;
            });
            
        }

        partial void _btnScaleMinus_TouchUpInside(UIButton sender)
        {
            _scale -= 0.2f;
            _v -= 0.4f;
           
            UIView.Animate(0.5f, () =>
            {
                _transform = CGAffineTransform.MakeScale(_scale, _scale);
                _seryozha.Transform = _transform;
            });
            
        }

        partial void _btnTransform_TouchUpInside(UIButton sender)
        {
            _isRect = !_isRect;
            UIView.Animate(0.5f, () =>
            {
                if (_isRect)
                    _seryozha.Layer.CornerRadius = _seryozha.Frame.Width / _v ;
                else
                    _seryozha.Layer.CornerRadius = 0;
            });
        }

        partial void _btnRotate_TouchUpInside(UIButton sender)
        {
            _rotation += ((float)Math.PI / 4);
            UIView.Animate(0.5f, () =>
            {;
                _transform.Rotate(_rotation);
                _seryozha.Transform = _transform;
            });
        }

        partial void _btnColor_TouchUpInside(UIButton sender)
        {
            UIView.Animate(0.5f, () =>
            {
                if (_seryozha.BackgroundColor == UIColor.Red)
                {
                    _seryozha.BackgroundColor = UIColor.Green;
                }
                else
                    _seryozha.BackgroundColor = UIColor.Red;
            });
        }

        #endregion

    }
}